<?php
/**
 *
 */
interface IDAOInci
{
  public function listarElementos();
  public function listarResponsables();
  public function listarTecnicos();
  public function actualizarIncidencia( $campo , $valor , $codigo );
  public function listarParadas();

}
