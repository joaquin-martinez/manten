<?php
/**
* Clase encargada de representar la averia.
*/
class Averia
{
  public $id;
  public $fallo;
  public $estado;
  public $kilos;
  public $duracion;
  public $elemento;
  public $causa;
  public $tecnico;
  public $firmaTec;
  public $responsable;
  public $firmaRes;
  public $limpieza;

  function __construct($kilos , $duracion , $id)
  {
    /*
    //obtengo un array con los parámetros enviados a la función
    $params = func_get_args();
    //saco el número de parámetros que estoy recibiendo
    $num_params = func_num_args();
    //cada constructor de un número dado de parámtros tendrá un nombre de	función
    //atendiendo al siguiente modelo __construct1() __construct2()...
    $funcion_constructor ='__construct'.$num_params;
    //compruebo si hay un constructor con ese número de parámetros
    if (method_exists($this,$funcion_constructor)) {
    //si existía esa función, la invoco, reenviando los parámetros que recibí en el constructor original
    call_user_func_array(array($this,$funcion_constructor),$params);
    */
    $this->kilos = $kilos;
    $this->duracion = $duracion;
    $this->id = $id;
  }

  function getMin(){
    $his = explode( ':' , $this->duracion["horaIni"] );
    $hfs =  explode( ':' , $this->duracion["horaFin"] );
    $min = ($hfs[0]*60 + $hfs[1]*1) - ($his[0]*60 + $his[1]*1);

    return $min;
  }
}





?>
