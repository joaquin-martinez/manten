<?php
class AccionSalir implements EjecutaAccion {

  function ejecutar(){
    session_start();
    session_destroy();
    require("../index.php");
  }

}
