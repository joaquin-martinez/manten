<?php
class Usuario{
  private $tipo;
  private $identificador;
  private $clave;

  function __construct( $tipo , $identificador , $clave ){
    $this->tipo = $tipo;
    $this->identificador = $identificador;
    $this->clave = $clave;
  }

  function getTipo(){
    return $this->tipo;
  }


  function getIdentificador(){
    return $this->identificador;
  }

  function getClave(){
    return $this->clave;
  }

  function setClave( $clave ){
    $this->clave = $clave;
  }

}
