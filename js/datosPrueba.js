{"dats" : [ {
  "codigoId" : "1234",
  "duracion" : {
    "horaini": "10:45",
    "horafin": "10:55"
  },
  "causaAveria": "Salto de los magneto.",
  "estado" : "abierto"

} ,


{
  "codigoId" : "2345",
  "duracion" : {
    "horaini": "11:45",
    "horafin": "11:55"
  },
  "causaAveria": "Atasco producto.",
  "estado" : "abierto"

} ,


{
  "codigoId" : "3456",
  "duracion" : {
    "horaini": "12:45",
    "horafin": "12:55"
  },
  "causaAveria": "Cortocircuito.",
  "estado" : "abierto"

} ,


{
  "codigoId" : "4567",
  "duracion" : {
    "horaini": "14:45",
    "horafin": "14:55"
  },
  "causaAveria": "Avería mecánica.",
  "estado" : "abierto"

}


]}
