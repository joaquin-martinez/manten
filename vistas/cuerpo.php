<?php
require_once("../modelo/Averia.php");

$kilos['kb'] = '9527';
$kilos['kn'] = 9393;
$kilos['kr'] = 8993;
$kilos['ktr'] = 10256;
$kilos['kh'] = 356;
$kilos['kp'] = 245;
$kilos['kg'] = 16;
$kilos['kte'] = '85025';

$duracion['horaIni']= "12:29" ;
$duracion['horaFin']= "12:49" ;

$averia= new Averia($kilos , $duracion , 100 );
// var_dump($kilos);
?>


<main>
  <?php
  echo '<div id="pag0">';
  require_once("listado.php");
  echo '</div>';
  echo '<div id="pag2">';
  require_once("cerrados.php");
  echo '</div>';
  echo '<div id="pag1">';
  // require_once('cuerpo.php');
  // echo '</div>';
  ?>
<h3>Detalle de parada</h3>
  <div class="izd" id="izd">

    <p> KILOS BRUTOS : <?= $averia->kilos['kb'] ?> KG</p>
    <p> KILOS NETOS : <?= $averia->kilos['kn'] ?> KG</p>
    <p> RESTOS : <?= $averia->kilos['kr'] ?> KG</p>
    <p> TOTAL KILOS REPROCESO : <?= $averia->kilos['ktr'] ?> KG</p>

  </div>

  <div class="der" id="der" >

    <p> KILOS DE HOY : <?= $averia->kilos['kh'] ?> KG</p>
    <p> TOTAL KILOS CAMBIO DE PALETS : <?= $averia->kilos['kp'] ?> KG</p>
    <p> TIRADA DE GENERO : <?= $averia->kilos['kg'] ?> KG</p>
    <p> TOTAL KILOS REPROCESO EXT : <?= $averia->kilos['kte'] ?> KG</p>

  </div>

  <table id="tabla">
    <thead>
      <tr>
        <th>EQUIPO / ELEMENTO</th>
        <th>TIEMPO PARADA (HORA)</th>
        <th>MINUTOS</th>
        <th>CAUSA</th>
        <th>FIRMA TECNICO</th>
        <th>LIMPIEZA</th>
        <th>FIRMA RESPONSABLE LINEA</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td id="element" >
        </td>
        <td id="t1" >
          <input type="time" class="usr_time" name="time_11" id="time_11" value= "<?= $duracion['horaIni'] ?>">
          <br>
          <input type="time" class="usr_time" name="time_12" id="time_12" value= "<?= $duracion['horaFin'] ?>">
        </td>
        <td id = "tmin"><?= $averia->getMin() ?></td>
        <td id="causa"></td>
        <td id="ft1" class = "casillat"></td>
        <td>
          <select class="" name="limpiar" id="limpiar">
            <option value="no">NO</option>
            <option value="si">SI</option>
          </select>

        </td>
        <td id="fr1" class = "casillar"></td>
      </tr>
      <!--
      <tr>
      <td>
      <select class="" name="" id="" multiple>
      <option value="Horno1">Horno 1</option>
      <option value="Horno2">Horno 2</option>
      <option value="Horno3">Horno 3</option>
      <option value="Horno4">Horno 4</option>
      <option value="Volquete1">Volquete 1</option>
      <option value="Volquete2">Volquete 2</option>
      <option value="Volquete3">Volquete 3</option>
      <option value="Volquete4">Volquete 4</option>
    </select></td>
    <td id="t1" >
    <input type="time" class="usr_time" name="time_21" id="time_21" value= "12:45">
    <br>
    <input type="time" class="usr_time" name="time_22" id="time_22" value= "12:49">
  </td>
  <td>4</td>
  <td> <input type="text" name="" value="NO REFLEJADA AJUSTE CAIDA Y SINCRO"> </td>
  <td id="ft2" class = "casillat"></td>
  <td>
  <select class="" name="">
  <option value="no">NO</option>
  <option value="si">SI</option>
</select>
</td>
<td id="fr2" class = "casillar"></td>
</tr>
<tr>
<td> <input type="text" name="" value="Horno 2"> </td>
<td>
<input type="time" class="usr_time" name="time_31" id="time_31" value= "12:45">
<br>
<input type="time" class="usr_time" name="time_32" id="time_32" value= "12:49">
</td>

<td>4</td>
<td>NO REFLEJADA AJUSTE CAIDA Y SINCRO</td>
<td id="ft3" class = "casillat"></td>
<td>
<select class="" name="">
<option value="no">NO</option>
<option value="si">SI</option>
</select>
</td>
<td id="fr3" class = "casillar"></td>
</tr>
-->
</tbody>

</table>

<button id="btn-cierre" >volver</button>


</div >
</main>
