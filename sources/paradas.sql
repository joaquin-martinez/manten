DROP DATABASE IF EXISTS paradasbd;
CREATE DATABASE paradasbd CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE paradasbd;

-- TABLAS
DROP TABLE IF EXISTS elementostb;
CREATE TABLE elementostb (
	elemento varchar(20) NOT NULL,
	PRIMARY KEY (elemento)
);

DROP TABLE IF EXISTS tecnicostb;
CREATE TABLE tecnicostb (
	id_tec int(6) NOT NULL AUTO_INCREMENT,
	tecnico varchar(15) NOT NULL,
	clavet varchar(15) NOT NULL,
	PRIMARY KEY (id_tec)
);

DROP TABLE IF EXISTS responsablestb;
CREATE TABLE responsablestb (
	id_res int(6) NOT NULL AUTO_INCREMENT,
	responsable varchar(15) NOT NULL,
	claver varchar(15) NOT NULL,
	PRIMARY KEY (id_res)
);


DROP TABLE IF EXISTS paradastb;
CREATE TABLE paradastb (
	codigoId varchar(6) NOT NULL ,
	fallo varchar(50) NOT NULL,
	estado varchar(50) NOT NULL,
	causaAveria varchar(50),
	elemento varchar(50),
	rubricar varchar(50),
	rubricat varchar(50),
	respon varchar(30),
	tecnico varchar(20),
	limpiar varchar(4),
	horaini varchar(16) NOT NULL,
	horafin varchar(16) NOT NULL,
	PRIMARY KEY (codigoId)

);


-- DATOS
INSERT INTO elementostb VALUES ('horno1'),('horno2'),('bascula'),('calibre'),('volquete1'),('volquete2'),('cinta1'),('cinta2');

INSERT INTO tecnicostb ( tecnico , clavet) VALUES
( 'juan' , '655' ),
('antonio' , '655' ),
('jaime' , '655' ),
( 'ramiro' , '655'),
('roberto' , '655'),
( 'raul' , '655' );

INSERT INTO responsablestb ( responsable , claver) VALUES
( 'ana' , '655' ),
('andrea' , '655' ),
('adela' , '655' ),
( 'alba' , '655'),
('almudena' , '655'),
( 'antonia' , '655' );

INSERT INTO paradastb ( codigoId ,fallo, estado, causaAveria, elemento, rubricar, rubricat, respon, tecnico, limpiar, horaini, horafin) VALUES
( 1234,'cortocircuito', 'abierto', '', '', '', '', '', '', '', '10:13' , '10:35'),
( 2345,'atasco', 'abierto', '', '', '', '', '', '', '', '10:23' , '10:45'),
( 3456,'magneto', 'abierto', '', '', '', '', '', '', '', '10:33' , '10:45'),
( 4567,'averia mecanica', 'abierto', '', '', '', '', '', '', '', '10:33' , '10:45'),
( 0123,'termopar', 'abierto', '', '', '', '', '', '', '', '10:33' , '10:45'),
( 1034,'salto diferencial', 'abierto', '', '', '', '', '', '', '', '10:33' , '10:45'),
( 1204,'cortocircuito', 'abierto', '', '', '', '', '', '', '', '10:33' , '10:45'),
( 1230,'cortocircuito', 'abierto', '', '', '', '', '', '', '', '10:33' , '10:45');
